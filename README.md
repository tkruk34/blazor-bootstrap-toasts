# blazor-bootstrap-toasts

Easy to use toast component based on bootstrap alerts. Only C# and CSS. No Javascript.

demo: http://toasts.kruki.at/

Nuget package: https://www.nuget.org/packages/BlazorBootstrapToasts/#

# how to use
There are 2 possibilities how to use blazor-bootstrap-toasts.

## use as single component
To use the toast component just insert the toast component like this:

```
<Toast @ref="Toast"></Toast>
```

Then add a reference to the toast in the @code-section like this:

```
public Toast Toast { get; set; }
```

Now you can call the from whereever you want in the scope like this:

```
Toast.Show("success","my message bla bla...",3000)
```

## use a singleton (as service)
First of all you have to register the included service in your program.cs like this:

_program.cs_

```
public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddSingleton<IToastService, ToastService>();

            await builder.Build().RunAsync();
        }
```

Then you have to add a toast component to the service. To use the Service in your whole application, just add it in the app.razor component.

```
@using BlazorBootstrapToasts

@inject IToastService ToastService

<Router AppAssembly="@typeof(Program).Assembly" PreferExactMatches="@true">
    <Found Context="routeData">
        <RouteView RouteData="@routeData" DefaultLayout="@typeof(MainLayout)" />
    </Found>
    <NotFound>
        <LayoutView Layout="@typeof(MainLayout)">
            <p>Sorry, there's nothing at this address.</p>
        </LayoutView>
    </NotFound>
</Router>

<Toast @ref="Toast"></Toast>

code{
    private Toast Toast { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (firstRender)
        {
            ToastService.RegisterComponent(Toast);
        }
    }
}
```

Now you can inject the service whereever you want and call the Show()-method on the service.

```
<button class="btn btn-block btn-success" onclick="@(() => {ToastService.Show("success", Message, Duration); })">Success</button>
```

# use custom item template
```
<Toast @ref="Toast">
    <ToastContent Context="message">
        My message in the custom toast: <b>@message</b>
    </ToastContent>
</Toast>
```
