﻿using System.Threading.Tasks;

namespace BlazorBootstrapToasts
{
    public interface IToastService
    {
        Toast Toast { get; }

        Task RegisterComponent(Toast toast);

        /// <summary>
        /// Display a toast-message.
        /// </summary>
        /// <param name="type">Typical bootstrap color type ("success","primary","danger","warning", ...)</param>
        /// <param name="message">Message to be displayed.</param>
        /// <param name="duration">Duration how long the toast lasts (ms) | min: 2000</param>
        /// <returns></returns>
        Task Show(string type, string message, int duration);

    }

    public class ToastService : IToastService
    {
        public Toast Toast { get; private set; }

        public async Task RegisterComponent(Toast toast)
        {
            Toast = toast;
        }

        public async Task Show(string type, string message, int duration)
        {
            await Toast.Show(type, message, duration);
        }
    }
}
