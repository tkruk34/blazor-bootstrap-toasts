﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlazorBootstrapToasts
{
    public struct ItemContent<T>
    {
        public T Item { get; }
        public ItemContent(T item)
        {
            Item = item;
        }
    }
}
