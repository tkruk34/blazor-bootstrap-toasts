﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlazorBootstrapToasts
{
    class ToastModel
    {
        public string Message { get; set; }
        public string AlertClass { get; set; }
        public string FadeOutClass { get; set; } = "";
        public bool AlreadyShown { get; set; }
    }
}
